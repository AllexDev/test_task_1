package com.alexdeww.testworkapp.entity

data class CurrencyInfo(
        val name: String = "",
        val volume: Int = 0,
        val price: Price = Price()

)