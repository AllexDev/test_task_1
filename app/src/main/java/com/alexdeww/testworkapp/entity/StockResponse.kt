package com.alexdeww.testworkapp.entity

class StockResponse(
        val stock: List<CurrencyInfo> = arrayListOf()
)