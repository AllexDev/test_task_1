package com.alexdeww.testworkapp.entity

data class Price(
        val amount: Double = 0.0
)