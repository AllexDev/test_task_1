package com.alexdeww.testworkapp.presentation.presenter

import com.alexdeww.testworkapp.entity.StockResponse
import com.alexdeww.testworkapp.presentation.view.MainView
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.rx2androidnetworking.Rx2AndroidNetworking
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

@InjectViewState
class MainPresenter : MvpPresenter<MainView>() {

    companion object {

        private const val URL = "http://phisix-api3.appspot.com/stocks.json"
        private const val REFRESH_INTERVAL = 15L //second

    }

    private val loadDataObserver = Rx2AndroidNetworking.get(URL)
            .build()
            .getObjectObservable(StockResponse::class.java)
            .map { it.stock }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { viewState.showProgressDialog() }
            .doOnEach { viewState.hideProgressDialog() }
            .share()

    private val refreshTimerSub = Observable.interval(REFRESH_INTERVAL, TimeUnit.SECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { updateData() }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        updateData()
    }

    override fun onDestroy() {
        refreshTimerSub?.dispose()
        super.onDestroy()
    }

    fun updateData() {
        loadDataObserver.subscribe(
                { viewState.updateData(it) },
                { viewState.showErrorLoadingData(it.message) }
        )
    }
}