package com.alexdeww.testworkapp.presentation.view

import com.alexdeww.testworkapp.entity.CurrencyInfo
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface MainView : MvpView {

    fun updateData(data: List<CurrencyInfo>)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showErrorLoadingData(message: String?)

    fun showProgressDialog()
    fun hideProgressDialog()

}