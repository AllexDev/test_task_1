package com.alexdeww.testworkapp.ui.activity

import android.os.Bundle
import android.view.Menu
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import com.alexdeww.testworkapp.R
import com.alexdeww.testworkapp.entity.CurrencyInfo
import com.alexdeww.testworkapp.presentation.presenter.MainPresenter
import com.alexdeww.testworkapp.presentation.view.MainView
import com.alexdeww.testworkapp.ui.adapter.CurrencyListAdapter
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : MvpAppCompatActivity(), MainView {

    @InjectPresenter lateinit var presenter: MainPresenter

    private val adapter = CurrencyListAdapter()
    private var progressDialog: MaterialDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        gvCurrency.adapter = adapter
    }

    override fun onDestroy() {
        hideProgressDialog()
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)

        menu?.findItem(R.id.menu_refresh)?.setOnMenuItemClickListener {
            presenter.updateData()
            true
        }

        return true
    }

    override fun updateData(data: List<CurrencyInfo>) {
        adapter.updateList(data)
    }

    override fun showErrorLoadingData(message: String?) {
        Toast.makeText(this, getString(R.string.error_load_data).format(message), Toast.LENGTH_SHORT).show()
    }

    override fun showProgressDialog() {
        progressDialog?.dismiss()
        progressDialog = MaterialDialog.Builder(this)
                .content(R.string.updating)
                .progress(true, 0)
                .cancelable(false)
                .show()
    }

    override fun hideProgressDialog() {
        progressDialog?.dismiss()
    }

}
