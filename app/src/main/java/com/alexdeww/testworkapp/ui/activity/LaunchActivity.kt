package com.alexdeww.testworkapp.ui.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

/**
 * Вообще можно обойтись без дополнительного экрана, просто задать тему LaunchTheme для MainActivity
 * в манифесте, а в onCreate поменять тему обратно
 */

class LaunchActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}