package com.alexdeww.testworkapp.ui.adapter

import android.view.View
import com.alexdeww.testworkapp.R
import com.alexdeww.testworkapp.entity.CurrencyInfo
import kotlinx.android.synthetic.main.currency_item.view.*

class CurrencyListAdapter : BaseTemplateAdapter<CurrencyInfo>() {

    companion object {
        private const val AMOUNT_FORMAT = "%.2f"
    }

    override fun getLayoutId(): Int = R.layout.currency_item

    override fun buildViewItem(v: View, item: CurrencyInfo) {
        v.tvCurrencyName.text = item.name
        v.tvVolume.text = item.volume.toString()
        v.tvAmount.text = AMOUNT_FORMAT.format(item.price.amount)
    }

}