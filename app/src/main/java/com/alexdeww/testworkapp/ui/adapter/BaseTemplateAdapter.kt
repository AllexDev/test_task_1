package com.alexdeww.testworkapp.ui.adapter

import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import java.util.ArrayList

abstract class BaseTemplateAdapter<ITEM_TYPE> : BaseAdapter() {

    protected var dataList = ArrayList<ITEM_TYPE>()

    @LayoutRes
    protected abstract fun getLayoutId(): Int

    protected abstract fun buildViewItem(v: View, item: ITEM_TYPE)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val v = convertView ?: LayoutInflater.from(parent?.context).inflate(getLayoutId(), parent, false)
        buildViewItem(v, dataList[position])
        return v
    }

    override fun getItem(position: Int): ITEM_TYPE = dataList[position]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getCount(): Int = dataList.size

    protected open fun clearList() {
        dataList.clear()
    }

    open fun updateList(list: List<ITEM_TYPE>) {
        clearList()
        dataList.addAll(list)
        notifyDataSetChanged()
    }

}